#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2019 Max Mehl

BINDIR="$(dirname "$(readlink -f "$0")")"
# Test if config.cfg exists and set needed variables
if [ ! -e "$BINDIR"/config.cfg ]; then echo "Missing config.cfg file. Edit and rename config.cfg.sample"; exit 1; fi
source "$BINDIR"/config.cfg

QUIET=false
RESET=false
while getopts qr OPT; do
  case $OPT in
    r)  RESET=true;;
    q)  QUIET=true;;
  esac
done

# Shift arguments based on amount of given options
shift "$((OPTIND-1))"

function print_usage {
  cat << EOF
INSTRUCTIONS FOR overtime-calc:

The following are the optional parameters which can be combined with
the time arguments (see below):

  -r    Reset stored last week's overtime
  -q    Less verbose and potentially helpful output


There are multiple ways how to use the script, depending on the amount
of given arguments:

overtime-calc 37:25

  Takes the configured weekly worktime (35:00 by default) and the
  stored overtime of last week to calculate this week's overtime. The
  first and only argument is this week's worktime. If the last week's
  overtime is not available, it asks for it. Result: 2:25


overtime-calc 37:25 3:37

  Takes the configured weekly worktime (35:00 by default). The first
  argument is this week's worktime, the second argument the overtime of
  last week. Result: 6:02


overtime-calc 37:25 3:37 32:00 

  Does not use any stored values. The first argument is this week's
  worktime, the second argument the overtime of last week, and the
  third argument the targeted weekly worktime according to your
  contract. Result: 9:02


Please note that you can combine them with -r or -q.
EOF

}

function calc_overtime {
  T_BEFORE=$2
  T_NOW=$1

  # If negative overtime, transform output for correct calculations, e.g. -1:20 to -1:-20
  if echo "${T_BEFORE}" | grep -q "-"; then
    T_BEFORE=$(echo ${T_BEFORE} | sed -E 's/^-([0-9]+):([0-9]+)/-\1:-\2/g')
  fi

  WEEKLY=$(echo ${WEEKLY} | awk -F: '{print ($1 * 60) + $2}')
  T_BEFORE=$(echo ${T_BEFORE} | awk -F: '{print ($1 * 60) + $2}')
  T_NOW=$(echo ${T_NOW} | awk -F: '{print ($1 * 60) + $2}')

  # Overtime this week
  OT_NOW=$((${T_NOW} - ${WEEKLY}))

  # New total overtime
  OT_NOW=$((${T_BEFORE} + ${OT_NOW}))

  ((hour=${OT_NOW}/60))
  ((min=${OT_NOW}-${hour}*60))

  if ! echo "${min}" | grep -E -q "[[:digit:]]{2}"; then
    min=$(echo "${min}" | sed -E "s/([[:digit:]]{1})/0\1/")
  fi

  OT_NOW="${hour}:${min}"

  # If negative overtime, transform output to transform e.g. -1:-20 to -1:20
  if echo "${OT_NOW}" | grep -q "-"; then
    OT_NOW=-$(echo ${OT_NOW} | sed 's/-//g')
  fi

  # Storing this overtime in file, and write to stdout
  echo "${OT_NOW}" > "${TMP}"
  echo "${OT_NOW}"
}

function reset_last {
  read -p "Please enter last week's overtime in HH:MM format: " OT_BEFORE
  # Storing last overtime in file
  echo "${OT_BEFORE}" > "${TMP}"
}

function out {
  if ! $QUIET; then
    echo "$@"
  fi
}

# file in temp dir to store the last overtime. If it exists, import value
TMP="$(dirname $(mktemp -u))"/last_overtime
if [[ -e "${TMP}" ]]; then
  OT_BEFORE=$(cat "${TMP}")
fi

# Reset overtime if requested
if $RESET; then
  reset_last
fi

# No arguments given
if [[ -z "$1" ]]; then
  echo "You provided no times to this script. Here are the set variables:"
  echo "Your weekly worktime: ${WEEKLY}"
  if [[ -z "${OT_BEFORE}" ]]; then
    echo "No recent overtime stored"
  else
    echo "Your last stored overtime is: ${OT_BEFORE}"
  fi
  echo
  print_usage
# 1 argument given
elif [[ -n "$1" ]] && [[ -z "$2" ]]; then
  if [[ -z "${OT_BEFORE}" ]]; then
    echo "No recent overtime stored"
    reset_last
  fi
  T_NOW=$1
  out "Calculate this week's overtime."
  out "${WEEKLY} weekly worktime, ${T_NOW} worked this week, ${OT_BEFORE} overtime from last week."
  OT_NOW=$(calc_overtime "${T_NOW}" "${OT_BEFORE}")
  out -n "Your new overtime: "
  echo "${OT_NOW}"
# 2 arguments given
elif [[ -n "$2" ]] && [[ -z "$3" ]]; then
  OT_BEFORE=$2
  T_NOW=$1
  out "Calculate this week's overtime with custom last week's overtime."
  out "${WEEKLY} weekly worktime, ${T_NOW} worked this week, ${OT_BEFORE} overtime from last week."
  OT_NOW=$(calc_overtime "${T_NOW}" "${OT_BEFORE}")
  out -n "Your new overtime: "
  echo "${OT_NOW}"
# 3 arguments given
elif [[ -n "$3" ]]; then
  WEEKLY=$3
  OT_BEFORE=$2
  T_NOW=$1
  out "Calculate this week's overtime with custom last week's overtime and custom weekly worktime."
  out "${WEEKLY} weekly worktime, ${T_NOW} worked this week, ${OT_BEFORE} overtime from last week."
  OT_NOW=$(calc_overtime "${T_NOW}" "${OT_BEFORE}")
  out -n "Your new overtime: "
  echo "${OT_NOW}"
fi
