<!--
SPDX-License-Identifier: GPL-3.0-or-later
SPDX-FileCopyrightText: 2019 Max Mehl
-->

# overtime-calc

This is a handy script to calculate one's overtime given the targeted weekly worktime, the working time of the current week, and the last week's overtime.

## Installation

Just clone this repository to your computer. Copy the `config.cfg.sample` file to `config.cfg` and adapt your weekly working time. The default is 35:00.

To simplify usage, you can link it to your PATH to call it from everywhere. In order to do so, run the following command in your terminal inside this project's directory:

`sudo ln -s $(readlink -f overtime-calc.sh) /usr/local/bin/overtime-calc`

Afterwards, you can run the script from everywhere by executing `overtime-calc` in your terminal.

## Usage

(Taken from the program's help)

```text
The following are the optional parameters which can be combined with
the time arguments (see below):

  -r    Reset stored last week's overtime
  -q    Less verbose and potentially helpful output


There are multiple ways how to use the script, depending on the amount
of given arguments:

overtime-calc 37:25

  Takes the configured weekly worktime (35:00 by default) and the
  stored overtime of last week to calculate this week's overtime. The
  first and only argument is this week's worktime. If the last week's
  overtime is not available, it asks for it. Result: 2:25


overtime-calc 37:25 3:37

  Takes the configured weekly worktime (35:00 by default). The first
  argument is this week's worktime, the second argument the overtime of
  last week. Result: 6:02


overtime-calc 37:25 3:37 32:00 

  Does not use any stored values. The first argument is this week's
  worktime, the second argument the overtime of last week, and the
  third argument the targeted weekly worktime according to your
  contract. Result: 9:02


Please note that you can combine them with -r or -q.
```

## License

GNU General Public License 3.0 or later (GPL-3.0-or-later)
